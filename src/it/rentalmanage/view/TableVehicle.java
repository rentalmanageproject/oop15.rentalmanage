package it.rentalmanage.view;

import it.rentalmanage.controller.ITableVehicleController;
import it.rentalmanage.controller.TableVehicleController;
import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

/**
 * Tabella che mostra l'elenco dei veicoli
 * @author nicolapanigucci
 */
public class TableVehicle extends JScrollPane {

    private Map<String, IVehicle> vehicleMap;
    private List<IVehicle> listVehicles;
    private JTable tbVehicles;
    private ITableVehicleController tableVehicleController;
    private ICustomTableModelVehicle customTableModelVehicle;

    /**
     * Costruttore di TableVehicle
     * @param prevPanel oggetto di tpo MainFrame
     * @param model oggetto di tipo IModel
     */
    public TableVehicle(final MainFrame prevPanel, final IModel model){

        this.customTableModelVehicle = new CustomTableModelVehicle();
        this.tbVehicles = new JTable(customTableModelVehicle);
        this.tableVehicleController = new TableVehicleController(model, customTableModelVehicle);
        tableVehicleController.showVehicle();
        tbVehicles.setFillsViewportHeight(true);

        this.setViewportView(tbVehicles);

        this.vehicleMap = model.getAllCar();

        listVehicles = new ArrayList<>();
        listVehicles =new ArrayList<>();
        Iterator<Map.Entry<String, IVehicle>> iterator = vehicleMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, IVehicle> entry = iterator.next();
            listVehicles.add(entry.getValue());
        }

        /**
         * Gestione doppio click sulla riga della tabella.
         * permette di visualizzare il pannello con tutte le informazioni del veicolo
         */
        tbVehicles.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int valueRow= tbVehicles.getSelectedRow();
                if((valueRow != -1) && (e.getClickCount() == 2)){

                    Collections.sort(listVehicles, (vehicle1, vehicle2) -> {
                        String vehicle1MM = vehicle1.getManufactorer().toLowerCase() + vehicle1.getModel().toLowerCase();
                        String vehicle2MM = vehicle2.getManufactorer().toLowerCase() + vehicle2.getModel().toLowerCase();
                        return vehicle1MM.compareTo(vehicle2MM);
                    });

                    prevPanel.setPanel(new ViewVehicle(prevPanel, listVehicles.get(valueRow), model));

                }

            }

        });

        revalidate();

    }
}
