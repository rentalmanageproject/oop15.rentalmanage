package it.rentalmanage.view;

import it.rentalmanage.controller.*;
import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Frame che permette di aggiornare le date di scadenza dei veicoli (assicurazione, bollo e revisione)
 * @author nicolapanigucci
 */
public class FrameExpiryDate extends JFrame {

    /**
     * Costruttore di FrameExpiryDate
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param vehicle oggetto di tipo IVehicle. Veicolo che ha delle scadenze imminenti
     */
    public FrameExpiryDate(final MainFrame prevPanel, final IModel model, final IVehicle vehicle){

        this.setTitle("Expiry dates");

        new dialog(prevPanel, model, vehicle);

    }

    private class dialog extends JDialog{

        private JLabel lblInsuranceCost;
        private JLabel lblInsuranceDate;
        private JLabel lblCarTaxCost;
        private JLabel lblCarTaxDate;
        private JLabel lblMotTestCost;
        private JLabel lblMotTestDate;

        private JButton btnSave;

        private JTextField tfInsuranceCost;
        private JTextField tfCarTaxCost;
        private JTextField tfMOTTestCost;
        private JTextField tfInsuranceDate;
        private JTextField tfCarTaxDate;
        private JTextField tfMOTTestDate;

        private GridBagConstraints gbc;
        private GridBagLayout gridBagLayout;

        private Expiring expiring;
        private SimpleDateFormat sdf;
        private IExpiring checkOnDate;

        private IVehicle vehicle;

        public dialog(final MainFrame prevPanel, final IModel model, final IVehicle vehicle){

            super(FrameExpiryDate.this);

            this.setSize(480, 170);
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.setModal(true);
            this.setResizable(false);
            this.setLocationRelativeTo(null);

            this.vehicle = vehicle;

            this.sdf = new SimpleDateFormat("dd/MM/yyyy");
            this.checkOnDate = new Expiring(null);

            this.gbc = new GridBagConstraints();

            this.gridBagLayout = new GridBagLayout();
            gridBagLayout.columnWidths = new int[]{140, 80, 0, 0, 0};
            gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
            gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
            gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
            this.setLayout(gridBagLayout);

            Insets insets = new Insets(5, 0, 5, 5);

            this.lblInsuranceCost = new JLabel("Insurance Cost : €");
            gbc.insets = insets;
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.anchor= GridBagConstraints.EAST;
            this.add(lblInsuranceCost, gbc);

            this.tfInsuranceCost = new JTextField(4);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = insets;
            gbc.gridx = 1;
            gbc.gridy = 0;
            this.add(tfInsuranceCost, gbc);

            this.lblInsuranceDate = new JLabel("Insurance Date :", JLabel.RIGHT);
            gbc.insets = insets;
            gbc.gridx = 2;
            gbc.gridy = 0;
            this.add(lblInsuranceDate, gbc);

            this.tfInsuranceDate = new JTextField(10);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = insets;
            gbc.gridx = 3;
            gbc.gridy = 0;
            this.add(tfInsuranceDate, gbc);

            this.lblCarTaxCost = new JLabel("Car Tax Cost : €", JLabel.RIGHT);
            gbc.insets = insets;
            gbc.gridx = 0;
            gbc.gridy = 1;
            this.add(lblCarTaxCost, gbc);

            this.tfCarTaxCost = new JTextField(4);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = insets;
            gbc.gridx = 1;
            gbc.gridy = 1;
            this.add(tfCarTaxCost, gbc);

            this.lblCarTaxDate = new JLabel("Car Tax Date :", JLabel.RIGHT);
            gbc.insets = insets;
            gbc.gridx = 2;
            gbc.gridy = 1;
            this.add(lblCarTaxDate, gbc);

            this.tfCarTaxDate = new JTextField(10);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = insets;
            gbc.gridx = 3;
            gbc.gridy = 1;
            this.add(tfCarTaxDate, gbc);

            this.lblMotTestCost = new JLabel("MOT Test Cost : €", JLabel.RIGHT);
            gbc.insets = insets;
            gbc.gridx = 0;
            gbc.gridy = 2;
            this.add(lblMotTestCost, gbc);

            this.tfMOTTestCost = new JTextField(4);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = insets;
            gbc.gridx = 1;
            gbc.gridy = 2;
            this.add(tfMOTTestCost, gbc);

            this.lblMotTestDate = new JLabel("MOT Test Date :", JLabel.RIGHT);
            gbc.insets = insets;
            gbc.gridx = 2;
            gbc.gridy = 2;
            this.add(lblMotTestDate, gbc);

            this.tfMOTTestDate = new JTextField(10);
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = insets;
            gbc.gridx = 3;
            gbc.gridy = 2;
            this.add(tfMOTTestDate, gbc);

            this.btnSave = new JButton("Save");
            gbc.gridx = 3;
            gbc.gridy = 3;
            this.add(btnSave, gbc);

            /**
             * Salva le nuove date se i campi sono corretti e chiudo il frame
             */
            btnSave.addActionListener(e -> {

                if (checkInput()){

                    Date insuranceDate = null;
                    Date carTaxDate = null;
                    Date motTestDate = null;

                    try {
                        insuranceDate = sdf.parse(tfInsuranceDate.getText());
                        carTaxDate = sdf.parse(tfCarTaxDate.getText());
                        motTestDate = sdf.parse(tfMOTTestDate.getText());

                        IVehicleController vehicleController = new VehicleController(model, vehicle);
                        vehicleController.updateVehicle(vehicle.getRentPrice(), vehicle.getRequestedLicense(), Integer.parseInt(tfInsuranceCost.getText()),
                                insuranceDate, Integer.parseInt(tfCarTaxCost.getText()), carTaxDate, Integer.parseInt(tfMOTTestCost.getText()),
                                motTestDate);

                        prevPanel.setPanel(new ViewVehicle(prevPanel, vehicle,model));

                    } catch (ParseException e1) {

                    }

                    this.dispose();

                }
            });

            /**
             * Setto i textfield
             */
            Calendar c = Calendar.getInstance();

            tfInsuranceCost.setText("" + this.vehicle.getInsuranceCost());
            c.setTime(this.vehicle.getInsuranceExpiring());
            tfInsuranceDate.setText("" + sdf.format(c.getTime()));
            tfCarTaxCost.setText("" + this.vehicle.getVehicleTaxCost());
            c.setTime(this.vehicle.getVehicleTaxDate());
            tfCarTaxDate.setText("" + sdf.format(c.getTime()));
            tfMOTTestCost.setText("" + this.vehicle.getMotTestCost());
            c.setTime(this.vehicle.getMotTestDate());
            tfMOTTestDate.setText("" + sdf.format(c.getTime()));

            /**
             * Evidenzia in rosso le label delle date scadute o che sono in procinto di scadere
             */
            expiring = new Expiring(vehicle);
            List<String> exp = expiring.checkExpiring();
            if (!exp.isEmpty()){
                if (exp.contains(expiring.getTextInsurance())){
                    lblInsuranceDate.setForeground(Color.RED);
                }
                if (exp.contains(expiring.getTextCarTax())){
                    lblCarTaxDate.setForeground(Color.RED);
                }
                if (exp.contains(expiring.getTextMOTTest())){
                    lblMotTestDate.setForeground(Color.RED);
                }
            }

            this.setVisible(true);

        }

        /**
         * Controllo se i valori inseriti sono corretti
         * @return true se tutti i valori sono corretti
         */
        private boolean checkInput(){

            String insuranceCost = tfInsuranceCost.getText();
            if (!insuranceCost.matches("\\A\\d+\\z") || Integer.parseInt(insuranceCost) == 0){
                JOptionPane.showMessageDialog(this, "Wrong Insurance Cost!");
                return false;
            }

            if (!checkOnDate.checkUpdateExpiringDateInput(vehicle.getInsuranceExpiring(), tfInsuranceDate.getText())){
                JOptionPane.showMessageDialog(this, "Wrong Insurance Date!");
                return false;
            }

            String carTaxCost = tfCarTaxCost.getText();
            if (!carTaxCost.matches("\\A\\d+\\z") || Integer.parseInt(carTaxCost) == 0){
                JOptionPane.showMessageDialog(this, "Wrong car Tax Cost!");
                return false;
            }

            if (!checkOnDate.checkUpdateExpiringDateInput(vehicle.getVehicleTaxDate(), tfCarTaxDate.getText())){
                JOptionPane.showMessageDialog(this, "Wrong car Tax Date!");
                return false;
            }

            String motTestCost = tfMOTTestCost.getText();
            if (!motTestCost.matches("\\A\\d+\\z") || Integer.parseInt(motTestCost) == 0){
                JOptionPane.showMessageDialog(this, "Wrong MOT Test Cost!");
                return false;
            }

            if (!checkOnDate.checkUpdateExpiringDateInput(vehicle.getMotTestDate(), tfMOTTestDate.getText())){
                JOptionPane.showMessageDialog(this, "Wrong MOT Test Date!");
                return false;
            }

            return true;
        }

    }

}
