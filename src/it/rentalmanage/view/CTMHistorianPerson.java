package it.rentalmanage.view;

import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.model.IRentedVehiclePeriod;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Implementazione del modello di TableHistorianPerson
 * @author nicolapanigucci
 * @see it.rentalmanage.view.ICTMHistorianPerson
 */
public class CTMHistorianPerson extends AbstractTableModel implements ICTMHistorianPerson {

    private List<IRentedVehiclePeriod> iRentedVehiclePeriods;
    private String[] header;
    private IModel model;
    private SimpleDateFormat sdf;
    private Calendar cal;

    /**
     * Costruttore di CTMHistorianPerson
     * @param model oggetto di tipo IModel
     */
    public CTMHistorianPerson(final IModel model){
        this.iRentedVehiclePeriods = new ArrayList<>();
        this.header = new String[]{"Number Plate", "Manufacturer", "Model", "Price", "Start Date", "End Date"};
        this.model = model;
        this.sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.sdf.setLenient(false);
    }

    @Override
    public int getRowCount() {
        return this.iRentedVehiclePeriods.size();
    }

    @Override
    public int getColumnCount() {
        return this.header.length;
    }

    @Override
    public String getColumnName(int column) {
        return this.header[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        String numberPlate = this.iRentedVehiclePeriods.get(rowIndex).getNumberPlate();

        switch (columnIndex){
            case 0:
                return numberPlate;
            case 1:
                return this.model.getCarFromNumPlateHistory(numberPlate).getManufactorer();
            case 2:
                return this.model.getCarFromNumPlateHistory(numberPlate).getModel();
            case 3:
                return this.iRentedVehiclePeriods.get(rowIndex).getRentedPrice();
            case 4:
                this.cal = Calendar.getInstance();
                this.cal.setTime(this.iRentedVehiclePeriods.get(rowIndex).getStartDate());
                return this.sdf.format(this.cal.getTime());
            case 5:
                this.cal = Calendar.getInstance();
                this.cal.setTime(this.iRentedVehiclePeriods.get(rowIndex).getEndDate());
                return this.sdf.format(this.cal.getTime());
        }

        return "";
    }

    @Override
    public void setElement(Map<String ,IPerson> historianPerson, IPerson person){

        Iterator<Map.Entry<String,IPerson>> iterator = historianPerson.entrySet().iterator();

        while (iterator.hasNext()){
            Map.Entry<String,IPerson> entry = iterator.next();
            String key = entry.getKey();

            if (key.equals(person.getFiscalCode())){
                setElement(person.getAllRentedVehicles());
            }
        }

    }

    /**
     * Ordina la collection di IRentedVehiclePeriod e la fa visualizzare nella tabella
     * @param rentedVehiclePeriods collection dei veicoli noleggiati da un cliente
     */
    private void setElement(Collection<IRentedVehiclePeriod> rentedVehiclePeriods){
        this.iRentedVehiclePeriods.clear();
        List<IRentedVehiclePeriod> list = new LinkedList<>(rentedVehiclePeriods);

        /**
         * ordinamento
         */
        Collections.sort(list, (vehicle1, vehicle2) -> {
            String vehicle1NS = this.model.getCarFromNumPlateHistory(vehicle1.getNumberPlate()).getManufactorer().toLowerCase() +
                    this.model.getCarFromNumPlateHistory(vehicle1.getNumberPlate()).getModel().toLowerCase();

            String vehicle2NS = this.model.getCarFromNumPlateHistory(vehicle1.getNumberPlate()).getManufactorer().toLowerCase() +
                    this.model.getCarFromNumPlateHistory(vehicle1.getNumberPlate()).getModel().toLowerCase();
            return vehicle1NS.compareTo(vehicle2NS);
        });

        this.iRentedVehiclePeriods.addAll(list);
    }
}
