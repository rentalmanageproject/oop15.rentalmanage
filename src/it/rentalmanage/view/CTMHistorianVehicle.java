package it.rentalmanage.view;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IRentalPeriod;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Implementazione del modello di TableHistorianVehicle
 * @author nicolapanigucci
 * @see it.rentalmanage.view.ICTMHistorianVehicle
 */
public class CTMHistorianVehicle extends AbstractTableModel implements ICTMHistorianVehicle {

    private List<IRentalPeriod> iRentalPeriods;
    private String[] header;
    private IModel model;
    private SimpleDateFormat sdf;
    private Calendar cal;

    /**
     * Costruttore di CTMHistorianVehicle
     * @param model oggetto di tipo IModel
     */
    public CTMHistorianVehicle(final IModel model){
        this.iRentalPeriods = new ArrayList<>();
        this.header = new String[]{"Fiscal Code", "Surname", "Name", "Start Date", "End Date"};
        this.model = model;
        this.sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.sdf.setLenient(false);
    }


    @Override
    public int getRowCount() {
        return this.iRentalPeriods.size();
    }

    @Override
    public int getColumnCount() {
        return this.header.length;
    }

    @Override
    public String getColumnName(int column) {
        return this.header[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        String fiscalCode = this.iRentalPeriods.get(rowIndex).getFCode();

        switch (columnIndex){
            case 0:
                return fiscalCode;
            case 1:
                return this.model.getPersonFromFCodeHistory(fiscalCode).getSurname();
            case 2:
                return this.model.getPersonFromFCodeHistory(fiscalCode).getName();
            case 3:
                this.cal = Calendar.getInstance();
                this.cal.setTime(this.iRentalPeriods.get(rowIndex).getStartDate());
                return this.sdf.format(this.cal.getTime());
            case 4:
                this.cal = Calendar.getInstance();
                this.cal.setTime(this.iRentalPeriods.get(rowIndex).getEndDate());
                return this.sdf.format(this.cal.getTime());
        }

        return "";
    }

    @Override
    public void setElement(Map<String, IVehicle> vehicleMap, IVehicle vehicle) {
        Iterator<Map.Entry<String,IVehicle>> iterator = vehicleMap.entrySet().iterator();

        while (iterator.hasNext()){
            Map.Entry<String,IVehicle> entry = iterator.next();
            String key = entry.getKey();

            if (key.equals(vehicle.getNumberPlate())){
                setElement(vehicle.getAllTenant());
            }
        }
    }

    /**
     * Ordina la collection di IRentalPeriod e la fa visualizzare nella tabella
     * @param rentalPeriodCollection collection dei clienti che hanno noleggiato un veicolo
     */
    private void setElement(Collection<IRentalPeriod> rentalPeriodCollection){
        this.iRentalPeriods.clear();
        List<IRentalPeriod> list = new LinkedList<>(rentalPeriodCollection);

        /**
         * ordinamento
         */
        Collections.sort(list, (person1, person2) -> {
            String person1NS = this.model.getPersonFromFCodeHistory(person1.getFCode()).getSurname().toLowerCase() +
                    this.model.getPersonFromFCodeHistory(person1.getFCode()).getName().toLowerCase();

            String person2NS = this.model.getPersonFromFCodeHistory(person2.getFCode()).getSurname().toLowerCase() +
                    this.model.getPersonFromFCodeHistory(person2.getFCode()).getName().toLowerCase();
            return person1NS.compareTo(person2NS);
        });

        this.iRentalPeriods.addAll(list);
    }


}
