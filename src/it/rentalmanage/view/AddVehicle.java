package it.rentalmanage.view;

import it.rentalmanage.controller.*;
import it.rentalmanage.model.*;
import it.rentalmanage.model.filemanager.JSonFileManager;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;
import java.util.*;

/**
 * Pannello che permette di registrare un nuovo veicolo
 * @author nicolapanigucci
 * @see it.rentalmanage.view.FormVehicles
 */
public class AddVehicle extends FormVehicles {

    private JLabel lblInsuranceCost;
    private JLabel lblInsuranceExpiring;
    private JLabel lblcarTaxCost;
    private JLabel lblcarTaxDate;
    private JLabel lblMOTTestCost;
    private JLabel lblMOTTestDate;

    private JTextField tfManufacturer;
    private JTextField tfModel;
    private JTextField tfRentPrice;
    private JTextField tfNumberPlate;
    private JTextField tfInsuranceCost;
    private JTextField tfInsuranceExpiring;
    private JTextField tfcarSeats;
    private JComboBox<DrivingLicense> cbDrvLicense;
    private JTextField tfcarTaxCost;
    private JTextField tfcarTaxDate;
    private JTextField tfMOTTestCost;
    private JTextField tfMOTTestDate;
    private Set<String> allNumbPlate;

    private IExpiring checkOnDate;

    /**
     * Costruttore di AddVehicle
     * @param prevPanel oggetto di tipo MainFrame
     * @param model oggetto di tipo IModel
     * @param allNumberPlate set delle targhe di tutti i veicoli registrati
     */
    public AddVehicle(final MainFrame prevPanel, final IModel model, final Set<String> allNumberPlate) {
        super(prevPanel, model);

        this.allNumbPlate = allNumberPlate;
        this.checkOnDate = new Expiring(null);

        panelTitle.add(new JLabel("ADD VEHICLE"));

        this.tfManufacturer = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.insets = otherLines;
        panelForm.add(tfManufacturer, c);

        this.tfModel = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 0;
        c.insets = otherLines;
        panelForm.add(tfModel,c);

        this.tfNumberPlate = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.insets = otherLines;
        panelForm.add(tfNumberPlate,c);

        this.tfcarSeats = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 1;
        c.insets = otherLines;
        panelForm.add(tfcarSeats,c);

        this.tfRentPrice = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 2;
        c.insets = firstLine;
        panelForm.add(tfRentPrice,c);

        this.cbDrvLicense = new JComboBox<>(DrivingLicense.values());
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 3;
        c.insets = otherLines;
        c.anchor = GridBagConstraints.CENTER;
        panelForm.add(cbDrvLicense,c);

        this.lblInsuranceCost = new JLabel("Insurance Cost : € ", JLabel.RIGHT);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 6;
        c.insets = firstLine;
        panelForm.add(lblInsuranceCost,c);

        this.tfInsuranceCost = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 6;
        c.insets = firstLine;
        panelForm.add(tfInsuranceCost,c);

        this.lblInsuranceExpiring = new JLabel("Insurance Expiring : ", JLabel.RIGHT);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 6;
        c.insets = firstLine;
        panelForm.add(lblInsuranceExpiring,c);

        this.tfInsuranceExpiring = new JTextField(15);
        this.tfInsuranceExpiring.setText("dd/MM/yyyy");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 6;
        c.insets = firstLine;
        panelForm.add(tfInsuranceExpiring,c);

        this.lblcarTaxCost = new JLabel("Car Tax Cost : € ", JLabel.RIGHT);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 7;
        c.insets = otherLines;
        panelForm.add(lblcarTaxCost,c);

        this.tfcarTaxCost = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 7;
        c.insets = otherLines;
        panelForm.add(tfcarTaxCost,c);

        this.lblcarTaxDate = new JLabel("Car Tax Date : ", JLabel.RIGHT);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 7;
        c.insets = otherLines;
        panelForm.add(lblcarTaxDate,c);

        this.tfcarTaxDate = new JTextField(15);
        this.tfcarTaxDate.setText("dd/MM/yyyy");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 7;
        c.insets = otherLines;
        panelForm.add(tfcarTaxDate,c);

        this.lblMOTTestCost = new JLabel("MotTest Cost : € ", JLabel.RIGHT);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 8;
        panelForm.add(lblMOTTestCost,c);

        this.tfMOTTestCost = new JTextField(15);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 8;
        panelForm.add(tfMOTTestCost,c);

        this.lblMOTTestDate = new JLabel("MotTest Date : ", JLabel.RIGHT);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 8;
        panelForm.add(lblMOTTestDate,c);

        this.tfMOTTestDate = new JTextField(15);
        this.tfMOTTestDate.setText("dd/MM/yyyy");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 8;
        panelForm.add(tfMOTTestDate,c);

        btnPay.setVisible(false);
        btnRemove.setVisible(false);

        /**
         * Salva il veicolo se i campi inseriti sono corretti
         */
        btnSaveModify.setText("Save");
        btnSaveModify.addActionListener(e -> {

            if(checkInput()){

                Date insuranceExpiring = null;
                Date motTestDate = null;
                Date carTaxDate = null;

                int motTestCost = Integer.parseInt(tfMOTTestCost.getText());
                int carTaxCost = Integer.parseInt(tfcarTaxCost.getText());
                int rentPrice = Integer.parseInt(tfRentPrice.getText());
                int carSeats = Integer.parseInt(tfcarSeats.getText());
                int insuranceCost = Integer.parseInt(tfInsuranceCost.getText());

                try {
                    motTestDate = sdf.parse(tfMOTTestDate.getText());
                    carTaxDate = sdf.parse(tfcarTaxDate.getText());
                    insuranceExpiring = sdf.parse(tfInsuranceExpiring.getText());

                    IVehicle vehicle = new Vehicle(new LinkedList<IRentalPeriod>(), motTestDate, motTestCost, carTaxDate, carTaxCost, rentPrice, (DrivingLicense) cbDrvLicense.getSelectedItem(),
                            carSeats, insuranceExpiring, insuranceCost, tfNumberPlate.getText(), tfManufacturer.getText(), tfModel.getText(), true);
                    IAddVehicleController controller = new AddVehicleController(new JSonFileManager(), model);
                    controller.writeVehicle(vehicle);

                } catch (ParseException e1) {

                }

                prevPanel.setPanel(new StorageVehicle(prevPanel, model));
            }
        });

        prevPanel.setVisibleBtnBackListener(true, e -> prevPanel.setPanel(new StorageVehicle(prevPanel, model)));

    }

    /**
     * Verifica se i dati inseriti sono validi
     * @return true se sono tutti validi
     */
    private boolean checkInput(){

        String manufacturer = tfManufacturer.getText();
        if(!manufacturer.matches("\\A[A-Za-z'\\\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong manufacturer's name!");
            return false;
        }

        String rentPrice = tfRentPrice.getText();
        if(!rentPrice.matches("\\A\\d+\\z") || Integer.parseInt(rentPrice) == 0) {
            JOptionPane.showMessageDialog(this, "Wrong rent price!");
            return false;
        }

        String numberPlate = tfNumberPlate.getText();
        if(!numberPlate.matches("\\A[A-Z0-9]{7}\\z")){
            JOptionPane.showMessageDialog(this, "Wrong number plate! Only capital characters and numbers");
            return false;
        }

        if(this.allNumbPlate.contains(numberPlate)){
            JOptionPane.showMessageDialog(this, "Number Plate already used!");
            return false;
        }

        String carSeats = tfcarSeats.getText();
        if(!carSeats.matches("\\A\\d+\\z") || Integer.parseInt(carSeats) == 0 || Integer.parseInt(carSeats) > 100) {
            JOptionPane.showMessageDialog(this, "Wrong car seats!");
            return false;
        }

        String model = tfModel.getText();
        if (!model.matches("\\A[\\w'\\\\ ]+\\z")){
            JOptionPane.showMessageDialog(this, "Wrong model!");
            return false;
        }

        if(cbDrvLicense.getSelectedIndex() == -1){
            JOptionPane.showMessageDialog(this, "Wrong license!");
            return false;
        }

        String insuranceCost = tfInsuranceCost.getText();
        if(!insuranceCost.matches("\\A\\d+\\z") || Integer.parseInt(insuranceCost) == 0) {
            JOptionPane.showMessageDialog(this, "Wrong insurance cost!");
            return false;
        }

        if (!checkOnDate.checkExpiringDateInput(tfInsuranceExpiring.getText())){
            JOptionPane.showMessageDialog(this, "Wrong insurance expiring date!");
            return false;
        }

        String carTaxCost = tfcarTaxCost.getText();
        if(!carTaxCost.matches("\\A\\d+\\z") || Integer.parseInt(carTaxCost) == 0) {
            JOptionPane.showMessageDialog(this, "Wrong car tax cost!");
            return false;
        }

        if (!checkOnDate.checkExpiringDateInput(tfcarTaxDate.getText())){
            JOptionPane.showMessageDialog(this, "Wrong car tax date!");
            return false;
        }

        String motTestCost = tfMOTTestCost.getText();
        if(!motTestCost.matches("\\A\\d+\\z") || Integer.parseInt(motTestCost) == 0) {
            JOptionPane.showMessageDialog(this, "Wrong MOT test cost!");
            return false;
        }

        if (!checkOnDate.checkExpiringDateInput(tfMOTTestDate.getText())){
            JOptionPane.showMessageDialog(this, "wrong MOT test date!");
            return false;
        }

        return true;
    }
}
