package it.rentalmanage.view;

import it.rentalmanage.controller.Expiring;
import it.rentalmanage.model.IVehicle;

import javax.swing.table.AbstractTableModel;
import java.util.*;

/**
 * Implementazione del modello di TableVehicle
 * @author nicolapanigucci
 * @see it.rentalmanage.view.ICustomTableModelVehicle
 */
public class CustomTableModelVehicle extends AbstractTableModel implements ICustomTableModelVehicle {

    private List<IVehicle> vehiclesList;
    private String[] header;

    /**
     * Costruttore di CustomTableModelVehicle
     */
    public CustomTableModelVehicle() {
        vehiclesList = new ArrayList<>();
        header = new String[]{"Manufactorer", "Model", "Number Plate", "Rentable", "Rent Price", "Expiry Date"};
    }

    @Override
    public int getRowCount() {
        return this.vehiclesList.size();
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public String getColumnName(int column) {
        return header[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex){
            case 0:
                return vehiclesList.get(rowIndex).getManufactorer();
            case 1:
                return vehiclesList.get(rowIndex).getModel();
            case 2:
                return vehiclesList.get(rowIndex).getNumberPlate();
            case 3:
                if(vehiclesList.get(rowIndex).isRentable()){
                    return new String("Rentable");
                }
                return new String("Not Rentable");
            case 4:
                return vehiclesList.get(rowIndex).getRentPrice();
            case 5:
                List<String> exp = new Expiring(vehiclesList.get(rowIndex)).checkExpiring();
                if (exp.size() == 0 ){
                    return "";
                }
                return exp;
        }

        return "";
    }

    @Override
    public void setElement(Map<String ,IVehicle> vehicleList){
        setElement(vehicleList.values());
    }

    /**
     * Ordina la Collection di IVehicle e la fa visualizzare nella lista
     * @param vehicleCollection collection di veicoli
     */
    private void setElement(Collection<IVehicle> vehicleCollection){
        vehiclesList.clear();
        List<IVehicle> vehicleList = new LinkedList<>(vehicleCollection);

        /**
         * ordinamento
         */
        Collections.sort(vehicleList, (vehicle1, vehicle2) -> {
            String vehicle1MM = vehicle1.getManufactorer().toLowerCase()+vehicle1.getModel().toLowerCase();
            String vehicle2MM = vehicle2.getManufactorer().toLowerCase()+vehicle2.getModel().toLowerCase();
            return vehicle1MM.compareTo(vehicle2MM);
        });

        vehiclesList.addAll(vehicleList);

    }

}
