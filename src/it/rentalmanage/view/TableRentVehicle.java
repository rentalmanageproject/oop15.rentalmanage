package it.rentalmanage.view;

import it.rentalmanage.controller.*;
import it.rentalmanage.model.*;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;

/**
 * Tabella che mostra i veicoli noleggiabili dalle persone registrate. L'utente può effettuare il noleggio assegnando
 * alla persona un veicolo
 * @author nicolapanigucci
 */
public class TableRentVehicle extends JScrollPane {

    private JLabel lblEndDate;
    private JTextField tfEndDate;

    private List<IVehicle> listRentableVehicles;
    private JTable tbRentableVehicles;
    private ITableRantableController tableRentController;
    private CustomTableModelRentable customTableModelRentable;

    /**
     * Costruttore di TableRentVehicle
     * @param prevPanel oggetto di tipo MainFrame
     * @param person oggetto di tipo IPerson. Persona che vuole noleggiare un veicolo
     * @param model oggetto di tipo IModel
     * @param vehicleList lista dei veicoli noleggiabili
     */
    public  TableRentVehicle(final MainFrame prevPanel, final IPerson person, final IModel model, final List<IVehicle> vehicleList){

        this.customTableModelRentable = new CustomTableModelRentable();
        this.tbRentableVehicles = new JTable(customTableModelRentable);
        this.tableRentController = new TableRentableController(model, customTableModelRentable);
        tableRentController.showRentableVehicle();
        tbRentableVehicles.setFillsViewportHeight(true);

        this.setViewportView(tbRentableVehicles);

        listRentableVehicles = new ArrayList<>(vehicleList);

        /**
         * Gestisce il doppio click sulla riga della tabella.
         * Con il doppio click assegno un auto al cliente se passano tutti i controlli
         */
        tbRentableVehicles.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);

                int valueRow = tbRentableVehicles.getSelectedRow();
                if((valueRow != -1) && (e.getClickCount() == 2)){

                    Collections.sort(listRentableVehicles, (vehicle1, vehicle2) -> {
                        String vehicle1MM = vehicle1.getManufactorer().toLowerCase() + vehicle1.getModel().toLowerCase();
                        String vehicle2MM = vehicle2.getManufactorer().toLowerCase() + vehicle2.getModel().toLowerCase();
                        return vehicle1MM.compareTo(vehicle2MM);
                    });

                    /**
                     * Controlla se la persona possiede la patente richiesta per il veicolo selezionato
                     */
                    IRentController rentController = new RentController(model, TableRentVehicle.this, person);

                    if(!rentController.canRentVehicle(listRentableVehicles.get(valueRow).getRequestedLicense())){

                        JOptionPane.showMessageDialog(TableRentVehicle.this, "You have not the licence! Choose another vehicle");
                        return;
                    }

                    /**
                     * Crea un pannello per inserire la data di fine noleggio
                     */
                    JPanel panel = new JPanel();
                    lblEndDate = new JLabel("End Date : ");
                    tfEndDate = new JTextField(10);
                    tfEndDate.setText("dd/MM/yyyy");
                    panel.add(lblEndDate);
                    panel.add(tfEndDate);

                    int isValidDate;
                    do {
                        int result = JOptionPane.showOptionDialog(null, panel, "Start/End Date", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);

                        isValidDate = rentController.rent(listRentableVehicles.get(valueRow), tfEndDate.getText(), result);

                    }while (isValidDate == -1);

                    prevPanel.setPanel(new ViewPerson(prevPanel, model, person, null));

                }

            }
        });

        revalidate();
    }

    public void message(String text){
        JOptionPane.showMessageDialog(this, text);
    }

}
