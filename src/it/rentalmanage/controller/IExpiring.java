package it.rentalmanage.controller;

import java.util.Date;
import java.util.List;

/**
 * Implementa i metodi per verificare le scadenze di un veicolo (assicurazione, bollo, revisione).
 * @author nicolapanigucci
 */
public interface IExpiring {

    /**
     * Verifica le scadenze di un veicolo
     * @return lista di stringhe con le scadenze di un veicolo
     */
    List<String> checkExpiring();

    /**
     * Verifica la validità di una data
     * @param date data da controllare
     * @return true se la data è valida
     */
    boolean checkExpiringDateInput(String date);

    /**
     * Verifica se la data è stata modificata. Se ha subito modifiche ne viene controllata la validità
     * @param oldDate data da aggiornare
     * @param date nuova data inserita
     * @return true se la data è valida
     */
    boolean checkUpdateExpiringDateInput(Date oldDate, String date);

    /**
     * metodo utilizzato per verificare la presenza della data tra la lista delle scadenze
     * @return stringa "Insurance"
     */
    String getTextInsurance();

    /**
     * metodo utilizzato per verificare la presenza della data tra la lista delle scadenze
     * @return stringa "Vehicle Tax"
     */
    String getTextCarTax();

    /**
     * metodo utilizzato per verificare la presenza della data tra la lista delle scadenze
     * @return "MOT Test"
     */
    String getTextMOTTest();
}
