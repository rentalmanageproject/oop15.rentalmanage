package it.rentalmanage.controller;

import it.rentalmanage.model.IVehicle;
import it.rentalmanage.model.IModel;
import it.rentalmanage.view.ICTMHistorianVehicle;

import java.util.Map;

/**
 * Permette di visualizzare lo storico dei clienti che hanno noleggiato un veicolo
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.ITableHistorianVehicleController
 **/
public class TableHistorianVehicleController implements ITableHistorianVehicleController {

    private IModel model;
    private ICTMHistorianVehicle modelHistorianVehicle;

    /**
     * Costruttore di TableHistorianVehicleController
     * @param model oggetto di tipo IModel
     * @param ctmHistorianVehicle oggetto di tipo ICTMHistorianVehicle
     */
    public TableHistorianVehicleController(final IModel model, final ICTMHistorianVehicle ctmHistorianVehicle){
        this.model = model;
        this.modelHistorianVehicle = ctmHistorianVehicle;
    }

    @Override
    public void showHistorianVehicle(IVehicle vehicle) {
        Map<String, IVehicle> historianVehicle = this.model.getAllCarsHistory();
        this.modelHistorianVehicle.setElement(historianVehicle, vehicle);
    }
}
