package it.rentalmanage.controller;

import it.rentalmanage.model.*;
import it.rentalmanage.model.filemanager.IFileManager;
import it.rentalmanage.model.filemanager.JSonFileManager;
import it.rentalmanage.view.TableRentVehicle;

import javax.swing.*;
import java.util.Date;
import java.util.List;

/**
 * Gestisce il noleggio di un veicolo
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.IRentController
 */
public class RentController implements IRentController{

    private IRentedVehiclePeriod rentedVehiclePeriod;
    private IRentalPeriod rentalPeriod;
    private IModel model;
    private ICheckOnRentDate checkOnRentDate;
    private TableRentVehicle view;
    private IPerson person;

    /**
     * Costruttore di RentController
     * @param model oggetto di tipo IModel
     * @param view
     * @param person oggetto di tipo IPerson. Cliente che vuole noleggiare un veicolo
     */
    public RentController(final IModel model, final TableRentVehicle view, final IPerson person){
        this.model = model;
        this.view = view;
        this.person = person;
    }

    @Override
    public boolean canRentVehicle(final DrivingLicense d){

        List<DrivingLicense> personList = person.getDrivingLicense();

        if (personList.contains(d)){
            return true;
        } else{

            if (d.equals(DrivingLicense.A1) &&
                    (personList.contains(DrivingLicense.A2) || personList.contains(DrivingLicense.A3))){
                return true;
            } else if (d.equals(DrivingLicense.A2) && personList.contains(DrivingLicense.A3)){
                return true;
            } else if (d.equals(DrivingLicense.B) && personList.contains(DrivingLicense.BE)){
                return true;
            } else if (d.equals(DrivingLicense.C) && personList.contains(DrivingLicense.CE)){
                return true;
            } else if (d.equals(DrivingLicense.D) && personList.contains(DrivingLicense.DE)){
                return true;
            } else {
                return false;
            }

        }

    }

    @Override
    public int rent(final IVehicle vehicle, String endDate, final int choice) {

        this.checkOnRentDate = new CheckOnRentDate(this.view);

        if (choice == JOptionPane.YES_OPTION){
            if (checkOnRentDate.rentDate(endDate)){
                rentVehicle(vehicle, this.person, checkOnRentDate.getCurrentDate(), checkOnRentDate.getEndDate());
                return 1;
            } else {
                return -1;
            }
        }
        return 0;

    }

    /**
     * Gestisce il noleggio sui file
     * @param vehicle oggetto di tipo IVehicle. Veicolo che si vuole noleggiare
     * @param person oggetto di tipo IPerson. Cliente che vuole noleggiare il veicolo
     * @param start data di inizio noleggio
     * @param end data di fine noleggio
     */
    private void rentVehicle(IVehicle vehicle, IPerson person, Date start, Date end){

        IFileManager fileManager = new JSonFileManager();

        rentedVehiclePeriod = new RentedVehiclePeriod(vehicle.getNumberPlate(), start, end, vehicle.getRentPrice());
        this.rentalPeriod = new RentalPeriod(person.getFiscalCode(), start, end);

        model.removePerson(person);

        /**
         * Assegno l'auto alla persona
         */
        person.addCar(rentedVehiclePeriod);

        /**
         * Assegno la persona all'auto
         */
        model.removeCar(vehicle);
        vehicle.setRentability(false);
        vehicle.addTenant(rentalPeriod);
        model.addPerson(person);
        model.addCar(vehicle);
        model.addCarToHistory(vehicle);
        model.addPersonToHistory(person);

        fileManager.writeList(fileManager.writePersonToJArray(model.getAllPersons().values()), "Person");
        fileManager.writeList(fileManager.writeVehicleToJArray(model.getAllCar().values()), "Vehicle");
        fileManager.writeList(fileManager.writePersonToJArray(model.getAllPersonsHistory().values()), "PersonHistory");
        fileManager.writeList(fileManager.writeVehicleToJArray(model.getAllCarsHistory().values()), "VehicleHistory");
    }

}
