package it.rentalmanage.controller;

import java.util.Date;

/**
 * Implementa i metodi che verificano la validità di una data di fine noleggio e la restituiscono.
 * @author nicolapanigucci
 */
public interface ICheckOnRentDate {

    /**
     * Controlla la validità della data di noleggio
     * @param date data di noleggio
     * @return true se la data è valida
     */
    boolean rentDate(String date);

    /**
     * Restiruisce la data attuale
     * @return la data attuale
     */
    Date getCurrentDate();

    /**
     * Restiruisce la data di fine noleggio
     * @return data di fine noleggio
     */
    Date getEndDate();

}
