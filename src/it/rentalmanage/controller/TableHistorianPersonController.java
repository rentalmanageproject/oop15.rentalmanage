package it.rentalmanage.controller;

import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.view.ICTMHistorianPerson;

import java.util.Map;

/**
 * Permette di visualizzare lo storico dei veicoli noleggiati da un cliente
 * @author nicolapanigucci
 * @see it.rentalmanage.controller.ITableHistorianPersonController
 */
public class TableHistorianPersonController implements ITableHistorianPersonController {

    private IModel model;
    private ICTMHistorianPerson modelHistorianPerson;

    /**
     * Costruttore di TableHistorianPersonController
     * @param model oggetto di tipo IModel
     * @param ctmHistorianPerson oggetto di tipo ICTMHistorianPerson
     */
    public  TableHistorianPersonController(final IModel model, final ICTMHistorianPerson ctmHistorianPerson){
        this.model = model;
        this.modelHistorianPerson = ctmHistorianPerson;
    }

    @Override
    public void showHistorianPerson(final IPerson person) {
        Map<String, IPerson> historianPerson = this.model.getAllPersonsHistory();
        this.modelHistorianPerson.setElement(historianPerson, person);
    }
}
