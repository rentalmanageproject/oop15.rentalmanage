package it.rentalmanage.controller;

import it.rentalmanage.model.DrivingLicense;
import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.model.IRentedVehiclePeriod;
import it.rentalmanage.model.filemanager.IFileManager;
import it.rentalmanage.model.filemanager.JSonFileManager;
import it.rentalmanage.view.MainFrame;
import it.rentalmanage.view.ModifyPerson;
import it.rentalmanage.view.StoragePerson;

import javax.swing.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Implementa l'interfaccia IPersonController
 * @author Jacopo Galosi.
 * @see it.rentalmanage.controller.IPersonController
 */
public class PersonController implements IPersonController {

    private IFileManager manager;
    private IPerson person;
    private IModel model;
    private Calendar calendar;

    /**
     * Costruttore di IPersonController
     * @param model oggetto di tipo model
     * @param person oggetto di tipo person
     */
    public PersonController(final IModel model, final IPerson person) {
        this.manager = new JSonFileManager();
        this.model = model;
        this.person = person;

        this.calendar = Calendar.getInstance();
        this.calendar.setTime(new Date());
    }

    @Override
    public void updatePerson(String surname, String name, String address, String telephone, List<DrivingLicense> drivingLicenses) {

        this.person.setSurname(surname);
        this.person.setName(name);
        this.person.setAddress(address);
        this.person.setPhoneNumber(telephone);
        this.person.setDrivingLicense(drivingLicenses);

        this.model.removePerson(person);
        model.addPerson(person);
        manager.writeList(manager.writePersonToJArray(model.getAllPersons().values()), "Person");

    }

    @Override
    public void deletePerson(final MainFrame prevPanel, final ModifyPerson modifyPerson, final int choice){

        if (choice == JOptionPane.YES_OPTION){
            boolean canDelete = true;
            Calendar c = Calendar.getInstance();

            for (IRentedVehiclePeriod rentedCarPeriod : this.person.getAllRentedVehicles()){

                c.setTime(rentedCarPeriod.getEndDate());

                if (c.after(this.calendar)){
                    canDelete = false;
                }
            }

            if (canDelete){
                this.model.removePerson(person);
                manager.writeList(manager.writePersonToJArray(model.getAllPersons().values()), "Person");
            } else {
                JOptionPane.showMessageDialog(modifyPerson, "Error! There is a car in rental!");
                return;
            }

            prevPanel.setPanel(new StoragePerson(prevPanel, this.model));

        }

    }

}
