package it.rentalmanage.controller;

import it.rentalmanage.model.DrivingLicense;

import java.util.List;

/**
 * Implementa i metodi che gestiscono l'aggiunta e l'eliminazione delle patenti
 * @author nicolapanigucci
 */
public interface IFormPersonController {

    /**
     * Aggiunge una patente alla lista di patenti possedute
     * @param allDLicense lista delle patenti possedute
     * @param d patente selezionata
     * @param birthDate data di nascita
     * @return lista aggiornata delle patenti possedute
     */
    List<DrivingLicense> addDriveLicenseToList(List<DrivingLicense> allDLicense, final DrivingLicense d, final String birthDate);

    /**
     * Rimuove una patente dalla lista delle patenti possedute
     * @param allDLicense lista delle patenti possedute
     * @param specificList lista delle patenti che richiedono la patente B
     * @param d patente selezionata
     * @return lista aggiornata delle patenti possedute
     */
    List<DrivingLicense> removeDriveLicenseFromList(List<DrivingLicense> allDLicense, final List<DrivingLicense> specificList, final DrivingLicense d);

}
