package it.rentalmanage.controller;

import it.rentalmanage.model.IModel;
import it.rentalmanage.model.IPerson;
import it.rentalmanage.view.ICustomTableModelPerson;

import java.util.Map;

/**
 * Implementa l'interfaccia ITablePersonController
 * @author Jacopo Galosi
 * @see it.rentalmanage.controller.ITablePersonController
 */
public class TablePersonController implements ITablePersonController {

    private IModel model;
    private ICustomTableModelPerson customTableModelPerson;

    /**
     * Costruttore di TablePersonController
     * @param model oggetto di tipo model
     * @param viewModel oggetto di tipo custom table model person
     */
    public TablePersonController(final IModel model, final ICustomTableModelPerson viewModel) {
        this.model = model;
        this.customTableModelPerson = viewModel;
    }

    @Override
    public void showPerson() {
        Map<String,IPerson> personMap = this.model.getAllPersons();
        this.customTableModelPerson.setElement(personMap);
    }
}
